;; timer.scm -- Timer service.
;; Copyright (C) 2024 Ludovic Courtès <ludo@gnu.org>
;;
;; This file is part of the GNU Shepherd.
;;
;; The GNU Shepherd is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; The GNU Shepherd is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.

(define-module (shepherd service timer)
  #:use-module (shepherd service)
  #:use-module (shepherd support)
  #:use-module (shepherd comm)
  #:use-module ((fibers) #:hide (sleep))
  #:use-module (fibers channels)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (ice-9 vlist)
  #:export (calendar-event
            calendar-event?
            calendar-event-months
            calendar-event-days-of-month
            calendar-event-days-of-week
            calendar-event-hours
            calendar-event-minutes
            calendar-event-seconds
            sexp->calendar-event
            cron-string->calendar-event

            next-calendar-event

            command
            command?
            command-arguments
            command-user
            command-group
            command-directory
            command-resource-limits
            command-environment-variables
            sexp->command

            make-timer-constructor
            make-timer-destructor

            trigger-timer
            timer-trigger-action))

;;; Commentary:
;;;
;;; This module implements constructors and destructors for "timers"--services
;;; that invoke commands periodically.
;;;
;;; Code:

(define-record-type <calendar-event>
  (%calendar-event seconds minutes hours days-of-month months days-of-week)
  calendar-event?
  (seconds       calendar-event-seconds)
  (minutes       calendar-event-minutes)
  (hours         calendar-event-hours)
  (days-of-month calendar-event-days-of-month)
  (months        calendar-event-months)
  (days-of-week  calendar-event-days-of-week))

(define-syntax define-weekday-symbolic-mapping
  (syntax-rules ()
    "Define @var{symbol->index} as a procedure that maps symbolic weekday
names to indexes, and @var{index->symbol} that does the opposite."
    ((_ symbol->index index->symbol (names ...))
     (begin
       (define symbol->index
         (let ((mapping (let loop ((lst '(names ...))
                                   (index 0)
                                   (result vlist-null))
                          (match lst
                            (()
                             result)
                            ((head . tail)
                             (loop tail
                                   (+ 1 index)
                                   (vhash-consq head index result)))))))
           (lambda (symbol)
             "Given @var{symbol}, a weekday, return its index or #f."
             (match (vhash-assq symbol mapping)
               (#f #f)
               ((_ . index) index)))))

       (define index->symbol
         (let ((mapping (vector 'names ...)))
           (lambda (index)
             "Given @var{index}, return the corresponding weekday symbol or #f."
             (and (>= index 0) (< index (vector-length mapping))
                  (vector-ref mapping index)))))))))

(define-weekday-symbolic-mapping
  weekday-symbol->index weekday-index->symbol
  (sunday monday tuesday wednesday thursday friday saturday))

(define any-minute (iota 60))
(define any-hour (iota 24))
(define any-day-of-month (iota 31 1))
(define any-day-of-week (map weekday-index->symbol (iota 7)))
(define any-month (iota 12 1))

(define-syntax validate-range
  (syntax-rules (-)
    ((_ lst min - max)
     (for-each (lambda (value)
                 (unless (and (>= value min) (<= value max))
                   (raise (condition
                           (&message
                            (message (format #f (l10n "calendar-event: ~a: \
~a: value out of range (~a-~a)")
                                             'lst value min max)))))))
               lst))))

(define (validate-days-of-week days-of-week)
  (for-each (lambda (value)
              (unless (memq value any-day-of-week)
                (raise (condition
                        (&message
                         (message (format #f (l10n "calendar-event: ~a: \
invalid day of week")
                                          value)))))))
            days-of-week))

(define* (calendar-event #:key
                         (seconds '(0))
                         (minutes any-minute)
                         (hours any-hour)
                         (days-of-week any-day-of-week)
                         (days-of-month any-day-of-month)
                         (months any-month))
  "Return a calendar event that obeys the given constraints."
  (validate-range seconds       0 - 59)
  (validate-range minutes       0 - 59)
  (validate-range hours         0 - 23)
  (validate-range days-of-month 1 - 31)
  (validate-range months        1 - 12)
  (validate-days-of-week days-of-week)

  (%calendar-event seconds minutes hours days-of-month months
                   (map weekday-symbol->index days-of-week)))

(define-syntax-rule (define-date-setter name getter)
  (define (name date value)
    (set-field date (getter) value)))

(define-date-setter set-date-nanosecond date-nanosecond)
(define-date-setter set-date-second date-second)
(define-date-setter set-date-minute date-minute)
(define-date-setter set-date-hour date-hour)
(define-date-setter set-date-day date-day)
(define-date-setter set-date-month date-month)
(define-date-setter set-date-year date-year)

(define (increment-year date)
  (set-date-year date (+ 1 (date-year date))))

(define (increment-month date)
  (if (< (date-month date) 12)
      (set-date-month date (+ (date-month date) 1))
      (set-date-month (increment-year date) 1)))

(define (increment-day date)
  (if (< (date-day date)
         (days-in-month (date-month date) (date-year date)))
      (set-date-day date (+ (date-day date) 1))
      (set-date-day (increment-month date) 1)))

(define (increment-hour date)
  (if (< (date-hour date) 23)
      (set-date-hour date (+ (date-hour date) 1))
      (set-date-hour (increment-day date) 0)))

(define (increment-minute date)
  (if (< (date-minute date) 59)
      (set-date-minute date (+ (date-minute date) 1))
      (set-date-minute (increment-hour date) 0)))

(define (increment-second date)
  (if (< (date-second date) 59)
      (set-date-second date (+ (date-second date) 1))
      (set-date-second (increment-minute date) 0)))

(define (days-in-month month year)
  "Return the number of days in @var{month} of @var{year}."
  (let* ((next-day (make-date 0 0 0 0
                              1 (modulo (+ 1 month) 12)
                              (if (= 12 month) (+ 1 year) year)
                              0))
         (time (date->time-utc next-day))
         (date (time-utc->date
                (make-time time-utc 0
                           (- (time-second time) 3600))
                0)))
    (date-day date)))

(define (sooner current max)
  "Return a two-argument procedure that returns true when its first argument
is closer to @var{current} than its second argument.  The distance to
@var{current} is computed modulo @var{max}."
  (define (distance value)
    (modulo (- value current) max))

  (lambda (value1 value2)
    (< (distance value1) (distance value2))))

(define (fit-month date months)
  (let loop ((candidates (sort months
                               (sooner (date-month date) 12))))
    (match candidates
      ((first . rest)
       (if (and (= first (date-month date))
                (> (date-day date) 1))
           (loop rest)
           (let ((next (if (>= first (date-month date))
                           date
                           (increment-year date))))
             (set-date-month next first)))))))

(define (fit-day date days weekdays)
  (define days*
    (if (eq? weekdays any-day-of-week)
        days
        (lset-intersection
         =
         days
         (week-days->month-days weekdays
                                (date-month date)
                                (date-year date)))))

  (let loop ((candidates (sort days*
                               (sooner (date-day date)
                                       (days-in-month (date-month date)
                                                      (date-year date))))))
    (match candidates
      ((first . rest)
       (if (and (= first (date-day date))
                (> (date-hour date) 0))
           (loop rest)
           (if (>= first (date-day date))
               (set-date-day date first)
               (let ((date (increment-month (set-date-day date 1))))
                 (fit-day date days weekdays))))))))

(define (fit-hour date hours)
  (let loop ((candidates (sort hours
                               (sooner (date-hour date) 24))))
    (match candidates
      ((first . rest)
       (if (and (= first (date-hour date))
                (> (date-minute date) 0))
           (loop rest)
           (let ((next (if (>= first (date-hour date))
                           date
                           (increment-day date))))
             (set-date-hour next first)))))))

(define (fit-minute date minutes)
  (let loop ((candidates (sort minutes
                               (sooner (date-minute date) 60))))
    (match candidates
      ((first . rest)
       (if (and (= first (date-minute date))
                (> (date-second date) 0))
           (loop rest)
           (let ((next (if (>= first (date-minute date))
                           date
                           (increment-hour date))))
             (set-date-minute next first)))))))

(define (fit-second date seconds)
  (let loop ((candidates (sort seconds
                               (sooner (date-second date) 60))))
    (match candidates
      ((first . rest)
       (if (and (= first (date-second date))
                (> (date-nanosecond date) 0))
           (loop rest)
           (let ((next (if (>= first (date-second date))
                           date
                           (increment-minute date))))
             (set-date-second next first)))))))

(define (week-days->month-days week-days month year)
  "Given @var{week-days}, a list of week-days (between 0 and 6, where 0 is
Sunday), return the corresponding list of days in @var{month} of @var{year}."
  (let loop ((date (make-date 0 0 0 0 1 month year 0))
             (days '()))
    (if (= (date-month date) month)
        (loop (increment-day date)
              (if (memv (date-week-day date) week-days)
                  (cons (date-day date) days)
                  days))
        (reverse days))))

(define (next-calendar-event event date)
  "Return the date following @var{date} that matches @var{event}, a calendar
event record."
  (define (month date)
    (if (memv (date-month date) (calendar-event-months event))
        date
        (fit-month date (calendar-event-months event))))

  (define (day date)
    (if (and (memv (date-day date)
                   (calendar-event-days-of-month event))
             (memv (date-week-day date)
                   (calendar-event-days-of-week event)))
        date
        (fit-day date
                 (calendar-event-days-of-month event)
                 (calendar-event-days-of-week event))))

  (define (hour date)
    (if (memv (date-hour date) (calendar-event-hours event))
        date
        (fit-hour date (calendar-event-hours event))))

  (define (minute date)
    (if (memv (date-minute date) (calendar-event-minutes event))
        date
        (fit-minute date (calendar-event-minutes event))))

  (define (second date)
    (if (memv (date-second date) (calendar-event-seconds event))
        date
        (fit-second date (calendar-event-seconds event))))

  (define (nanosecond date)
    ;; Clear nanoseconds and jump to the next second.
    (increment-second (set-date-nanosecond date 0)))

  ;; TODO: Adjust timezone.
  (month (day (hour (minute (second (nanosecond date)))))))

(define (seconds-to-wait event)
  "Return the number of seconds to wait before the next occurrence of
@var{event} (the result is an inexact number, always greater than zero)."
  (let* ((now (current-time time-utc))
         (then (next-calendar-event event (time-utc->date now)))
         (diff (time-difference (date->time-utc then) now)))
    (+ (time-second diff)
       (/ (time-nanosecond diff) 1e9))))

(define (cron-string->calendar-event str)
  "Convert @var{str}, which contains a Vixie cron date line, into the
corresponding @code{calendar-event}.  Raise an error if @var{str} is invalid.

A valid cron date line consists of 5 space-separated fields: minute, hour, day
of month, month, and day of week.  Each field can be an integer, or a
comma-separate list of integers, or a range.  Ranges are represented by two
integers separated by a hyphen, optionally followed by slash and a number of
repetitions.  Here are examples:

@table @code
@item 30 4 1,15 * *
4:30AM on the 1st and 15th of each month;
@item 5 0 * * *
five minutes after midnight, every day;
@item 23 0-23/2 * * 1-5
23 minutes after the hour every two hour, on weekdays.
@end table"
  (define not-comma
    (char-set-complement (char-set #\,)))
  (define not-hyphen
    (char-set-complement (char-set #\-)))

  (define (parse-component component count min)
    (define (in-range? n)
      (and (integer? n)
           (>= n min) (< n (+ min count))))

    (define (range->numbers str)
      (let ((str step (match (string-index str #\/)
                        (#f (values str 1))
                        (index
                         (values (string-take str index)
                                 (string->number
                                  (string-drop str (+ 1 index))))))))
        (match (string-tokenize str not-hyphen)
          (((= string->number min) (= string->number max))
           (and (>= max min)
                (in-range? min) (in-range? max)
                (iota (floor-quotient (+ 1 (- max min)) step)
                      min step)))
          (((= string->number n))
           (and (in-range? n) (list n)))
          (_ #f))))

    (match component
      ("*" (if (= 7 count)                        ;days of week?
               *unspecified*
               (iota count min)))
      (str (match (string-tokenize str not-comma)
             (((= range->numbers numbers) ...)
              (and (every list? numbers)
                   (let ((numbers (concatenate numbers)))
                     (if (= 7 count)
                         (map weekday-index->symbol numbers)
                         numbers))))
             (_ #f)))))

  (define (fail component)
    (raise (condition
            (&message
             (message (format #f "~s: invalid ~a cron field"
                              str component))))))

  (match (string-tokenize str)
    ((minutes hours days-of-month months days-of-week)
     (letrec-syntax ((parse (syntax-rules ()
                              ((_ ((id count min) rest ...) args)
                               (let ((id (parse-component id count min)))
                                 (if id
                                     (parse (rest ...)
                                            (if (unspecified? id)
                                                args
                                                (cons* (symbol->keyword 'id) id
                                                       args)))
                                     (fail 'id))))
                              ((_ () args)
                               (apply calendar-event args)))))
       (parse ((minutes 60 0) (hours 60 0)
               (days-of-month 31 1) (months 12 1) (days-of-week 7 0))
              '())))
    (_
     (raise (condition
             (&message
              (message (format #f "~s: wrong number of cron date fields"
                               str))))))))


;;;
;;; Timer services.
;;;

;; Timer value returned by 'make-timer-constructor'.
(define-record-type <timer>
  (timer channel event action)
  timer?
  (channel timer-channel)                         ;channel
  (event   timer-event)                           ;<calendar-event>
  (action  timer-action))                         ;<command> | procedure

(define (print-timer timer port)
  "Print @var{timer} to @var{port}, omitting its @code{channel} field for
conciseness."
  (format port "#<timer ~s ~s ~a>"
          (timer-event timer) (timer-action timer)
          (number->string (object-address timer) 16)))

(set-record-type-printer! <timer> print-timer)

;; Command to be executed by a timer.
(define-record-type <command>
  (%command arguments user group environment-variables directory
            resource-limits)
  command?
  (arguments command-arguments)
  (user      command-user)
  (group     command-group)
  (environment-variables command-environment-variables)
  (directory command-directory)
  (resource-limits command-resource-limits))

(define* (command arguments #:key user group
                  (environment-variables (default-environment-variables))
                  (directory (default-service-directory))
                  (resource-limits '()))
  "Return a new command for @var{arguments}, a program name and argument
list, to be executed as @var{user} and @var{group}, with the given
@var{environment-variables}, in @var{directory}, and with the given
@var{resource-limits}."
  (%command arguments user group environment-variables directory
            resource-limits))

(define (calendar-event->sexp event)
  `(calendar-event (version 0)
                   (seconds ,(calendar-event-seconds event))
                   (minutes ,(calendar-event-minutes event))
                   (hours ,(calendar-event-hours event))
                   (days-of-month ,(calendar-event-days-of-month event))
                   (days-of-week ,(calendar-event-days-of-week event))
                   (months ,(calendar-event-months event))))

(define (sexp->calendar-event sexp)
  "Return the calendar event deserialized from @var{sexp}.  Return #f if
@var{sexp} is not recognized as a valid calendar event sexp."
  (match sexp
    (`(calendar-event (version 0)
                      (seconds ,seconds)
                      (minutes ,minutes)
                      (hours ,hours)
                      (days-of-month ,days-of-month)
                      (days-of-week ,days-of-week)
                      (months ,months))
     (%calendar-event seconds minutes hours days-of-month months
                      days-of-week))
    (_ #f)))

(define (command->sexp command)
  `(command (version 0)
            (arguments ,(command-arguments command))
            (user ,(command-user command))
            (group ,(command-group command))
            (environment-variables ,(command-environment-variables command))
            (directory ,(command-directory command))
            (resource-limits ,(command-resource-limits command))))

(define (sexp->command sexp)
  "Deserialize @var{sexp} into a command and return it.  Return #f if
@var{sexp} was not recognized."
  (match sexp
    (('command ('version 0)
               ('arguments arguments)
               ('user user) ('group group)
               ('environment-variables environment-variables)
               ('directory directory)
               ('resource-limits resource-limits)
               _ ...)
     (command arguments #:user user #:group group
              #:environment-variables environment-variables
              #:directory directory
              #:resource-limits resource-limits))
    (_ #f)))

(define-record-type-serializer (serialize-timer (timer <timer>))
  ;; Serialize TIMER to clients can inspect it.
  `(timer (version 0)
          (event ,(match (timer-event timer)
                    ((? calendar-event? event)
                     (calendar-event->sexp event))
                    (_ #f)))
          (action ,(match (timer-action timer)
                     ((? command? command) (command->sexp command))
                     (_ 'procedure)))
          (processes ,(timer-processes timer))
          (past-runs ,(ring-buffer->list (timer-past-runs timer)))))

(define (timer-request message)
  (lambda (timer)
    "Send @var{message} to @var{timer} and return its reply."
    (let ((reply (make-channel)))
      (put-message (timer-channel timer) `(,message ,reply))
      (get-message reply))))

(define timer-processes
  ;; Return the list of PID/start time pairs of the currently running
  ;; processes started by the given timer.
  (timer-request 'processes))

(define timer-past-runs
  ;; Return the list of past runs as a ring buffer.  Each run has the form
  ;; (STATUS END START).  When the timer's action is a command, STATUS is an
  ;; integer, its exit status; otherwise, STATUS is either 'success or
  ;; '(exception ...).  END and START are the completion and start times,
  ;; respectively, as integers (seconds since the Epoch).
  (timer-request 'past-runs))

(define sleep (@ (fibers) sleep))

(define %past-run-log-size
  ;; Maximum number of entries the log of timer runs.
  50)

(define* (make-timer-constructor event action
                                 #:key log-file
                                 wait-for-termination?)
  "Return a procedure for use as the @code{start} method of a service.  The
procedure will perform @var{action} at every occurrence of @code{event}, a
calendar event as returned by @code{calendar-event}.  @var{action} may be
either a command (returned by @code{command}) or a thunk; in the latter case,
the thunk must be suspendable or it could block the whole shepherd process.

When @var{log-file} is true, log the output of @var{action} to that file
rather than in the global shepherd log.

When @var{wait-for-termination?} is true, wait until @var{action} has finished
before considering executing it again; otherwise, perform @var{action}
strictly on every occurrence of @var{event}, at the risk of having multiple
instances running concurrently."
  (lambda ()
    (let ((channel (make-channel))
          (name (service-canonical-name (current-service))))
      (spawn-fiber
       (lambda ()
         (let-loop loop ((processes '())          ;PID/start time
                         (past-runs (ring-buffer %past-run-log-size))
                         (termination #f))
           (match (if (or termination
                          (and (pair? processes) wait-for-termination?))
                      (get-message channel)
                      (get-message* channel (seconds-to-wait event)
                                    'timeout 'overslept))
             (('terminate reply)
              ;; Terminate this timer and its processes.  Send #t on REPLY
              ;; when we're done.
              (local-output
               (l10n "Terminating timer '~a' with ~a process running."
                     "Terminating timer '~a' with ~a processes running."
                     (length processes))
               name (length processes))
              (for-each (match-lambda
                          ((pid . _)
                           (terminate-process pid SIGHUP)))
                        processes)
              ;; If there are processes left, keep going until they're gone.
              (if (pair? processes)
                  (loop (termination reply))
                  (put-message reply #t)))
             (('process-terminated pid status)
              ;; Process PID completed.
              (let ((start-time (assoc-ref processes pid))
                    (end-time ((@ (guile) current-time)))
                    (remaining (alist-delete pid processes)))
                (local-output
                 (l10n "Process ~a of timer '~a' terminated with status ~a \
after ~a seconds.")
                 pid name status
                 (- end-time start-time))
                (if (and termination (null? remaining))
                    (put-message termination #t)  ;done
                    (loop (processes remaining)
                          (past-runs
                           (ring-buffer-insert
                            (list status end-time start-time)
                            past-runs))))))
             (('processes reply)
              (put-message reply processes)
              (loop))
             (('past-runs reply)
              (put-message reply past-runs)
              (loop))
             ('timeout
              ;; Time to perform ACTION.
              (if (command? action)
                  (let ((pid status (start-command
                                     (command-arguments action)
                                     #:log-file log-file
                                     #:user (command-user action)
                                     #:group (command-group action)
                                     #:environment-variables
                                     (command-environment-variables action)
                                     #:directory (command-directory action)
                                     #:resource-limits
                                     (command-resource-limits action))))
                    (spawn-fiber
                     (lambda ()
                       (let ((status (get-message status)))
                         (put-message channel
                                      `(process-terminated ,pid ,status)))))

                    (local-output (l10n "Timer '~a' spawned process ~a.")
                                  name pid)
                    (loop (processes
                           (alist-cons pid ((@ (guile) current-time))
                                       processes))))
                  (let ((start-time ((@ (guile) current-time))))
                    (define result
                      (catch #t
                        (lambda ()
                          (action)
                          'success)
                        (lambda (key . args)
                          (local-output
                           (l10n "Exception caught while calling action of \
timer '~a': ~s")
                           name (cons key args))
                          `(exception ,key ,@args))))

                    (loop (past-runs
                           (ring-buffer-insert
                            (list result ((@ (guile) current-time)) start-time)
                            past-runs))))))

             ('overslept
              ;; Reached when resuming from sleep state: we slept
              ;; significantly more than the requested number of seconds.  To
              ;; avoid triggering every timer when resuming from sleep state,
              ;; sleep again to remain in sync.
              (local-output (l10n "Waiting anew for timer '~a' (resuming \
from sleep state?).")
                            name)
              (loop))))))

      (timer channel event action))))

(define (make-timer-destructor)
  "Return a procedure for the @code{stop} method of a service whose
constructor was given by @code{make-timer-destructor}."
  (lambda (timer)
    (let ((reply (make-channel)))
      (put-message (timer-channel timer) `(terminate ,reply))
      ;; Wait until child processes have terminated.
      (get-message reply))
    #f))

(define (trigger-timer timer)
  "Trigger the action associated with @var{timer} as if it had reached its
next calendar event."
  (local-output (l10n "Triggering timer."))
  (put-message (timer-channel timer) 'timeout))

(define timer-trigger-action
  (action 'trigger trigger-timer
          "Trigger the action associated with this timer."))
