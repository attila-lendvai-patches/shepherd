# GNU Shepherd --- Test log rotation.
# Copyright © 2024 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of the GNU Shepherd.
#
# The GNU Shepherd is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# The GNU Shepherd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.

shepherd --version
herd --version

socket="t-socket-$$"
conf="t-conf-$$"
log="t-log-$$"
pid="t-pid-$$"
service_log1="$PWD/t-service-log1-$$"
service_log2="$PWD/t-service-log2-$$"
external_log="$PWD/t-service-extlog-$$"

herd="herd -s $socket"

trap "zcat $log* || true;
      rm -f $socket $conf $log* $service_log1* $service_log2* $external_log*;
      test -f $pid && kill \`cat $pid\` || true; rm -f $pid" EXIT

cat > "$conf" <<EOF
(use-modules (shepherd service log-rotation))

(define command
  '("$SHELL" "-c" "while true; do echo logging things; sleep 0.2; done"))

(%default-rotation-size-threshold 0)

(define services
  (list (service '(one)
		 #:start (make-forkexec-constructor
                          command
                          #:log-file "$service_log1")
		 #:stop (make-kill-destructor))
        (service '(two)
		 #:start (make-forkexec-constructor
                          '("sleep" "600")
                          #:log-file "$service_log2")
		 #:stop (make-kill-destructor))
        (log-rotation-service
          #:external-log-files '("$external_log")
          #:rotation-size-threshold 0)))

(register-services services)
EOF

rm -f "$pid"
shepherd -I -s "$socket" -c "$conf" -l "$log" --pid="$pid" &

# Wait till it's ready.
while ! test -f "$pid" ; do sleep 0.3 ; done

$herd start one
$herd start two
$herd start log-rotation

sleep 0.5

for file in "$service_log1" "$service_log2" "$external_log" "$log"
do
    $herd files log-rotation | grep "$file"
done

test -f "$service_log1"
test -f "$service_log2"
echo "This is an external log file." > "$external_log"

# First rotation.
$herd trigger log-rotation

until grep "Rotated " "$log"; do sleep 0.5; done

test -f "$service_log1"
test -f "$service_log1.1.gz"
test -f "$service_log2.1.gz" && false
test -f "$service_log2"
test -f "$log"
test -f "$log.1.gz"

until test -f "$external_log.1.gz"; do sleep 0.5; done
gunzip < "$external_log.1.gz" | grep "external log file"
test -f "$external_log"
guile -c "(exit (zero? (stat:size (stat \"$external_log\"))))"

# Second rotation.
$herd trigger log-rotation

until test -f "$service_log1.2.gz"; do sleep 0.5; done
until test -f "$service_log1.1.gz"; do sleep 0.5; done
until test -f "$log.2.gz"; do sleep 0.5; done
test -f "$service_log1"
test -f "$service_log2.1.gz" && false

# Third rotation, with deletion of old log file.
touch -d "2017-10-01" "$service_log1.2.gz"
$herd trigger log-rotation

# The "Deleting ..." message might end up in the current $log or in $log.1.gz,
# depending on the order in which they are rotated.
until zgrep "Deleting .*$service_log1.2.gz" "$log"*; do sleep 0.2; tail -10 "$log"; done
until test -f "$service_log1.2.gz"; do sleep 0.2; done
until test -f "$service_log1.1.gz"; do sleep 0.2; done
test -f "$service_log1.3.gz" && false

$herd stop log-rotation
