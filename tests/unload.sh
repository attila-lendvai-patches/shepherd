# GNU Shepherd --- Ensure enloading a service works.
# Copyright © 2024 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of the GNU Shepherd.
#
# The GNU Shepherd is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# The GNU Shepherd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.

shepherd --version
herd --version

socket="t-socket-$$"
conf="t-conf-$$"
log="t-log-$$"
pid="t-pid-$$"

herd="herd -s $socket"

trap "rm -f $socket $conf $log;
      test -f $pid && kill \`cat $pid\` || true; rm -f $pid" EXIT

cat > "$conf" <<EOF
(register-services
 (list (service
	 '(a)
	 #:start (const #t))))

(start-service (lookup-service 'a))
EOF

rm -f "$pid" "$socket"
shepherd -I -s "$socket" -c "$conf" --pid="$pid" --logfile="$log" &

while ! test -f "$pid"; do sleep 0.5 ; done

$herd status
$herd status a | grep enabled

# Cause 'a' to have a replacement.
$herd load root "$conf"

# Unloading 'a' should stop it without starting its replacement.
$herd unload root a

$herd status a && false
$herd status

test "$($herd status | grep '^ ' | wc -l)" -eq 1

grep "Service a is now stopped" "$log"
